package com.gustibimo.learnspring.applicationcatalog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ApplicationCatalogApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationCatalogApplication.class, args);
	}
}
