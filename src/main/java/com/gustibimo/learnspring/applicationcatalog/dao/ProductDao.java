package com.gustibimo.learnspring.applicationcatalog.dao;

import com.gustibimo.learnspring.applicationcatalog.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {


}
